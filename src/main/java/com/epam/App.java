package com.epam;

import com.epam.task2.controller.Controller;
import com.epam.task2.model.MyFileReader;
import com.epam.task2.view.View;

import java.io.IOException;

public class App {
    public static void main(String[] args) throws IOException {
        try {
            Controller controller = new Controller(new View(), new MyFileReader());
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}

