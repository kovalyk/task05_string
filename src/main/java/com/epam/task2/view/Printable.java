package com.epam.task2.view;

import java.io.IOException;

@FunctionalInterface
public interface Printable {
    void print() throws IOException;

}
